<?php
require 'vendor/autoload.php';
use \giftbox\controller\ControllerGestion;
use \giftbox\controller\ControllerCadeaux;
use \giftbox\controller\ControllerCatalogue;
use \giftbox\controller\ControllerPanier;
use \giftbox\controller\ControllerCagnotte;
use \giftbox\models\Prestation;
use \giftbox\models\Categorie;
use \giftbox\models\Connexion;
Connexion::getInstance();
session_start();
$app=new Slim\Slim();

$app->get('/',function(){})->name('css');
$app->get('/',function(){})->name('js');
$app->get('/img/',function(){})->name('img');

$app->get('/', function(){
	(new ControllerCatalogue())->debut();	
})->name('racine');

$app->get('/catalogue/', function(){
	(new ControllerCatalogue())->catalogue();
})->name('catalogue');

$app->get('/cataloguetrie/', function(){
	(new ControllerCatalogue())->trierall();
})->name('cataloguetrie');

$app->get('/prestation/:id', function($id){
	(new ControllerCatalogue())->Presta($id);
})->name('prestaid');

$app->get('/categorie/:id', function($id){
	(new ControllerCatalogue())->ListePresta($id);
})->name('catid');

$app->get('/allcategories/', function(){
	(new ControllerCatalogue())->listeCat();
})->name('cat');

$app->get('/panier/:id',function($id){
	(new ControllerPanier())->Ajoutpanier($id);
})->name('ajout');

$app->get('/Panier/:p',function($p){
	(new ControllerPanier())->supprimer($p);
})->name('supp');

$app->get('/paniers/:id',function($id){
	(new ControllerPanier())->diminuer($id);
})->name('diminuer');

$app->get('/validation/',function(){
	(new ControllerPanier())->vali();
})->name('validation');

$app->get('/inscription/',function(){
	(new ControllerPanier())->inscription();
})->name('inscription');

$app->post('/recap/',function(){
	(new ControllerPanier())->recap();
})->name('recap');

$app->post('/url/',function(){
	(new ControllerPanier())->insertion();
})->name('url');

$app->get('/gestion/',function(){
	(new ControllerGestion())->donnee();
})->name('gestion');

$app->get('/Gestion/',function(){
	(new ControllerGestion())->coffret();
})->name('coffret');

$app->post('/Gestion/',function(){
	(new ControllerGestion())->coffret();
})->name('erreur');

$app->get('/cadeaux/:id',function($id){
	(new ControllerCadeaux())->cadeaux($id);
})->name('cadeaux');

$app->get('/cagnotte/:id',function($id){
	(new ControllerCagnotte())->recup_coffret($id);
})->name('cagnotte');

$app->get('/Cagnotte/:id',function($id){
	(new ControllerGestion())->supprimer($id);
})->name('supprimer');

$app->post('/cagnottep/:id',function($id){
	(new ControllerCagnotte())->ajouter($id);
})->name('paiementp');

$app->get('/cloture/',function(){
	(new ControllerGestion())->validation();
})->name('ValiC');

$app->get('/note/:id',function($id){
	(new controllerCatalogue())->aff_note($id);
})->name('affn');

$app->post('/catalogues/',function(){
	(new controllerCatalogue())->notation();
})->name('note');

$app->get('/accueil/',function(){
	(new controllerCatalogue())->Accueil();
})->name('acceuil');

$app->get('/admin/',function(){
	(new controllerCatalogue())->login();
})->name('admin');

$app->post('/gestionadmin/',function(){
	(new controllerCatalogue())->Administration();
})->name('choixad');

$app->get('/adjp/',function(){
	(new controllerCatalogue())->adjpinf();
})->name('adj');

$app->get('/suppPresta/',function(){
	(new controllerCatalogue())->suppPresta();
})->name('suppp');

$app->get('/desactiverPresta/',function(){
	(new controllerCatalogue())->desacPresta();
})->name('desacp');

$app->post('/enregistrementAdj/',function(){
	(new controllerCatalogue())->newpresta();
})->name('enregistrementAdj');

$app->post('/enregistrementSupp/',function(){
	(new controllerCatalogue())->supp();
})->name('enregistrementSupp');

$app->post('/enregistrementDesac/',function(){
	(new controllerCatalogue())->desac();
})->name('enregistrementDesac');

$app->run();




?>