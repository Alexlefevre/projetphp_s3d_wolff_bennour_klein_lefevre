function surligne(champ, erreur){
if(erreur)
	champ.style.backgroundColor = "#FF0000";
else
	champ.style.backgroundColor = "#00FF00";
}

function verifnom(champ){
	if(champ.value.length < 3 || champ.value.length > 25){
		surligne(champ,true);
		
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifprenom(champ){
	if(champ.value.length < 3 || champ.value.length > 25){
		surligne(champ, true);
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifmess(champ){
	if(champ.value.length < 3 || champ.value.length > 200){
		surligne(champ, true);
		alert("Veuillez faire un message de moins de 200 caractères.");
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifmdp(champ){
	if(champ.value.length < 3|| champ.value.length > 25){
		surligne(champ, true);
		
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}


function verifMail(champ){
	var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;

   if(!regex.test(champ.value)){
		surligne(champ, true);
		
		return false;
	}

   else{
		surligne(champ, false);
		return true;
	}
}

function verifcarte(champ){
	var carte = parseInt(champ.value);
	if(champ.value.length < 16 || champ.value.length > 16||isNaN(carte) ){
		surligne(champ,true);
		
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifmois(champ){
	var mois = parseInt(champ.value);
	if(champ.value.length < 2 || champ.value.length > 2||isNaN(mois)|| mois<01 || mois>12 ){
		surligne(champ,true);
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifannee(champ){
	var annee = parseInt(champ.value);
	if(champ.value.length <4 || champ.value.length > 4||isNaN(annee)|| annee<1900 || annee>2017){
		surligne(champ,true);
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifcrypto(champ){
	var crypto = parseInt(champ.value);
	if(champ.value.length <3 || champ.value.length > 3||isNaN(crypto)){
		surligne(champ,true);
		return false;
	}
	else {
		surligne(champ, false);
		return true;
	}
}

function verifpaiement(f){

   var carteOk = verifcarte(f.carte);
   var moisOk = verifmois(f.mois);
   var anneeOk = verifannee(f.annee);
   var cryptoOk = verifcrypto(f.crypto);

   if(carteOk && moisOk && anneeOk && cryptoOk)
      return true;
   else{
	   alert("Veuillez remplir correctement tous les champs.");
      return false;
   }
}

function verifpaiementcag(f){

   var carteOk = verifcarte(f.carte);
   var moisOk = verifmois(f.mois);
   var anneeOk = verifannee(f.annee);
   var cryptoOk = verifcrypto(f.crypto);
   var entierOk = verifentier(f.entier);

   if(carteOk && moisOk && anneeOk && cryptoOk && entierOk)
      return true;
   else{
	   alert("Veuillez remplir correctement tous les champs.");
      return false;
   }
}

function verifForm(f){

   var nomOk = verifnom(f.nom);
   var prenomOk = verifprenom(f.prenom);
   var mailOk = verifMail(f.mail);
   var messOk = verifmess(f.message);
    var mdpOk = verifmdp(f.mdp);

   if(nomOk && prenomOk && mailOk && messOk && mdpOk)
      return true;
   else{
	   alert("Veuillez remplir correctement tous les champs.");
      return false;
   }
}

function verifconnexion(f){

   var mailOk = verifMail(f.mail);
    var mdpOk = verifmdp(f.mdp);

   if(mailOk && mdpOk)
      return true;
   else{
	   alert("Veuillez remplir correctement tous les champs.");
      return false;
   }
}

function verifentier(champ){
   var entier = parseInt(champ.value);
   if(isNaN(entier) || entier < 0 || entier > 1000)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verifnote(champ){
   var note = parseInt(champ.value);
   if(isNaN(note) || note < 0 || note > 5)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verifn(f){

   var noteOk = verifnote(f.note);

   if(noteOk){
      return true;
   }
   else{
	   alert("Veuillez remplir correctement tous les champs.");
      return false;
   }
}