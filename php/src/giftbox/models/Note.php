<?php

namespace giftbox\models;
	
class Note extends \Illuminate\Database\Eloquent\Model{
	protected $table='note';
	protected $primaryKey='id';
	public $timestamps=false;
}
?>