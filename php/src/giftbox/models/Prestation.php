<?php
namespace giftbox\models;

class Prestation extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'prestation';
	protected $primaryKey ='id';
	public $timestamps = false ;
	
}
?>