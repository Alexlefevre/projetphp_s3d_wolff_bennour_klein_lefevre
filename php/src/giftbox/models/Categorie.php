<?php
namespace giftbox\models;

class Categorie extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'categorie';
	protected $primaryKey = 'id';
	public $timstamps = false;
	
	public function presta(){
		return $this->hasMany('giftbox\models\Prestation','cat_id');
	}
}
?>	