<?php
namespace giftbox\models;
require 'vendor/password.php';
use giftbox\models\Client;
use giftbox\models\Coffret;
use \giftbox\vue\VueGestion;

class Authentification{
	
	public static function createUser($mail,$mdp,$id){
		$hash=password_hash($mdp, PASSWORD_DEFAULT);
		$c=Client::find($id);
		$c->email=$mail;
		$c->mdp=$hash;
		$c->save();	
	}
	
	public static function authentification($mail,$mdp,$hash){
		$erreur=null;
		$c=Client::where('email','=',$mail)->get();
		if(!isset($_SESSION['coffret'])){
			$_SESSION['coffret']=array();
			}
		if(password_verify($mdp, $hash)) {
			$_SESSION['coffret']=$c;

		} 
		else {
			$erreur='<h3>mot de passe incorret ou login incorrect</h3>';
			
		}
		return $erreur;
	}
	
	public static function loadProfile($mail){
	$id=null;
	if(!isset($_SESSION['profil'])){
			$_SESSION['profil']=array();
			}
	foreach($_SESSION['coffret'] as $k1=>$v1){
		$id=$v1->id;
	}
	$c=Coffret::where('client_id','=',$id)->get();
	$_SESSION['profil']=$c;
}

public static function checkAccessRights($required){
	$erreud=null;
	if($required!='admin'||$required==null){
		$erreud='<h3>Vous avez pas la permission</h3>';
	}
	return $erreud;

}
}
?>