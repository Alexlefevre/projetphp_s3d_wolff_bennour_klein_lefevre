<?php

namespace giftbox\models;
use Illuminate\Database\Capsule\Manager as DB;

class Connexion{
	
	private static $inst=null;

	public function __construct(){
$db=new DB();
$db->addConnection(parse_ini_file('src/conf/config.ini'));
$db->setAsGlobal();
$db->bootEloquent();
	}

public static function getInstance() {
	
		if(is_null(self::$inst)) {
			self::$inst = new Connexion();
		}
	
		return self::$inst;
	}

}

?>