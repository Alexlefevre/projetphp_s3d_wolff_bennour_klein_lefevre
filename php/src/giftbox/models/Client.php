<?php
namespace giftbox\models;

class Client extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'client';
	protected $primaryKey ='id';
	public $timestamps = false ;
	
}
?>