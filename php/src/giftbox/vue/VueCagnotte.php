<?php

namespace giftbox\vue; 

class VueCagnotte{

	const CAGNOTTE=1;
	CONST ADJ=3;
	protected $t;
	
	function __construct($liste,$mon=null,$id=null)
	{
		$this->t=$liste;
		$this->mon=$mon;
		$this->id=$id;
	}

public function affichage_coffret(){
	$app=\Slim\Slim::getInstance();
		$tp=0;
		$i=0;
		$html="
		<table cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($this->t as $k1=>$v1){
				$html.="<tr>
					<td>".$v1->nom.'<br>'."</td>
					<td>".$v1->descr.'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1->img."'><br></td>
					<td>".$v1->prix."€<br></td>
					<td>".$v1->quantite."<br></td>";
				$tp=$tp+$v1->prix*$v1->quantite;
				$i++;
		}

		$html.=" </table>
		<h3>prix total : ".$tp."€</h3>
		<h3>Somme déjà payée : ".$this->mon."€
		";
		$html.="<h2 class='titre'>Paiement : </h2>
		<form class='form-style-9' action='".$app->urlFor('paiementp',array('id'=>$this->id))."' onsubmit='return verifpaiementcag(this)' method='post'>
		<p>
		<ul>
		<li>
		Pays : 
		<select name='choix'>
			<option value='choix1'>France</option>
			<option value='choix2'>Espagne</option>
			<option value='choix3'>Royaume-Uni</option>
			<option value='choix4'>Canada</option>
			<option value='choix5'>Etats-Unis</option>
			<option value='choix6'>Chine</option>
			<option value='choix7'>Japon</option>
		</select>
		</li>
		<li>
		N° de carte : 
		<input type='text' name='carte' class='field-style field-full align-none' onblur='verifcarte(this)' />
		</li>
		<li>
		Modes de paiement : 
		<select name='choix'>
			<option value='choix1'>VISA</option>
			<option value='choix2'>MasterCard</option>
		</select>
		</li>
		<li>
		Date d'expiration :
		</li>
		<li>
		<input type='text' name='mois' class='field-style field-split align-left' onblur='verifmois(this)' placeholder='Mois' size=5/>/<input type='text' name='annee' class='field-style field-split align-right' onblur='verifannee(this)' placeholder='année' size=5 />
		</li>
		<li>
		Cryptogramme visuel :
		<input type='text' name='crypto' class='field-style field-full align-none' onblur='verifcrypto(this)' size=5/><br><br>
		</li>
		<li>
		Montant :
		</li>
		<li>
		<input type='number' name='mont' class='field-style field-full align-none' onblur='verifentier(this)'/>
		</li>
		<li>
		<input type='submit' value='Valider' />
		</li>
		</ul>
		</p>
		</form>
		";
		
		return $html;
	}
	
	public function coffret(){
		$html='<h1 class="titre_vali">paiement reussi</h1>
		<h3>vous avez participé à hauteur de '.$this->t.'€</h3>';
		return $html;
	}
	
	public function entete(){
		$app=\Slim\Slim::getInstance();
		$html='<header>
					<ul id="nav">
						<li><a href="'.$app->urlFor('catalogue').'">Catalogue</a></li>
						<li><a href="'.$app->urlFor('cat').'">Categories</a></li>
					</ul>
			  </header>';
		
		return $html;
	}
	
	public function render($selecteur)
	{
		$app=\Slim\Slim::getInstance();
		$css=$app->urlFor('css');
		$js=$app->urlFor('js');
		$entete=$this->entete();
		$html;
		
		switch($selecteur)
		{
			case self::CAGNOTTE:
			$html=$this->affichage_coffret();
			break;
			
			
			case self::ADJ:
			$html=$this->coffret();
			break;
		}
		
		$structure= <<<END
			<!DOCTYPE html>
				<html>
					<head>
						<meta charset="UTF-8">
						<title>Panier</title>
						<script type="text/javascript" src="$js/accueil.js"></script>
						<link rel="stylesheet" href="$css/accueil.css">
					</head>
					<body>
						$entete
						$html
						</body>
						</html>
END;
		
		echo $structure;
	}
}
?>