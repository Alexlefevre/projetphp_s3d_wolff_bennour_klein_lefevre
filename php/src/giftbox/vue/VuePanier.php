<?php
namespace giftbox\vue; 


class VuePanier{
	const PANIER=1;
	const SUPP=2;
	const DIMI=3;
	const VALI=4;
	const INSCRIP=5;
	const RECAP=6;
	const URL=7;
	protected $t;
	
	function __construct($liste)
	{
		$this->t=$liste;
	}

	public function panier(){
		$app=\Slim\Slim::getInstance();
		$tp=0;
		$i=0;
		$req = $_SESSION['panier'];
		$html="<h2>Panier :</h2>
		<table id='panier' cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($req as $k1=>$v1){
				$html.="<tr>
					<td>".$v1->nom.'<br>'."</td>
					<td>".$v1->descr.'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1->img."'><br></td>
					<td>".$v1->prix."€<br></td>
					<td><a href='".$app->urlFor('ajout',array('id'=>$v1->id))."'><button class='B3'>+</button></a><br>".$v1->quantite."<br><a href='".$app->urlFor('diminuer',array('id'=>$v1->id))."'><button class='B3'>-</button></a></td>
					<td><a href='".$app->urlFor('supp',array('p'=>$i))."'><button class='B2'>supprimer</button></a></td>
					</tr>";	
					
				$tp=$tp+$v1->prix*$v1->quantite;
				$i++;
		}
		$html.="</table><h3>prix totale : ".$tp."€</h3><br>
		";
		$html.="<a href='".$app->urlFor('validation')."'><button class='B2'>Validation</button></a>";
		
		return $html;
	}
	
	public  function validation(){
		$app=\Slim\Slim::getInstance();
		$req = $_SESSION['panier'];
		$html="<h2>Votre Panier :</h2>
		<table id='panier' cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>catégorie</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($req as $k1=>$v1){
				$html.="<tr>
					<td>".$v1->nom.'<br>'."</td>
					<td>".$v1->descr.'<br>'."</td>		
					<td>".$v1->categorie.'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1->img."'><br></td>
					<td>".$v1->prix."€<br></td>
					<td>".$v1->quantite.'<br>'."</td>
					";
		}		
			$html.='</table>'.$this->t;
		return $html;
	}
	
	public function saisie(){
		$app=\Slim\Slim::getInstance();
		$html='<h2 class="titre">Inscription</h2>
			<form class="form-style-9" action="'.$app->urlFor('recap').'"onsubmit="return verifForm(this)" method="post">
			<ul>
			<li>
			<input type="text" name="nom" onblur="verifnom(this)" class="field-style field-split align-left" placeholder="Nom" />
			<input type="text" name="prenom" onblur="verifnom(this)"  class="field-style field-split align-right" placeholder="Prénom" />
			</li>
			<li>
			<input type="email" name="mail" onblur="verifMail(this)" class="field-style field-split align-left" placeholder="Email" />
			<input type="password" name="mdp" onblur="verifmdp(this)" class="field-style field-split align-right" placeholder="Mot de passe" />
			</li>
			<li>
			<textarea type="text" name="message" onblur="verifmess(this)" placeholder="Message" class="field-style"></textarea>
			</li>
			<li>
			<select name="paiement">
				<option>Classique</option>
				<option>Cagnotte</option>
			</select>
			</li>
			<li>
			<input type="submit" value="Valider Inscription" />
			</li>
			</ul>
			</form>';
			return $html;
	}
	
	public function resumer(){
		$app=\Slim\Slim::getInstance();
		$tp=0;
		$i=0;
		$req = $_SESSION['panier'];
		$html="<h2 class='titre'>Votre Panier :</h2>
		<table cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($req as $k1=>$v1){
				$html.="<tr>
					<td>".$v1->nom.'<br>'."</td>
					<td>".$v1->descr.'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1->img."'><br></td>
					<td>".$v1->prix."€<br></td>
					<td>".$v1->quantite."<br></td>
					</tr>";
				$tp=$tp+$v1->prix*$v1->quantite;
				$i++;
		}
		$html.=" </table>
		<h3>Mode de paiement : ".$_POST['paiement']."</h3>
		<h3>Prix total : ".$tp."€</h3><br>";
		
		if($_POST['paiement']=='Classique'){
		$html.="<h2 class='titre'>Paiement : </h2>
		
		<form class='form-style-9' action='".$app->urlFor('url')."' onsubmit='return verifpaiement(this)' method='post'>
		<p>
		<ul>
		<li>
		Pays : 
		<select name='choix'>
			<option value='choix1'>France</option>
			<option value='choix2'>Espagne</option>
			<option value='choix3'>Royaume-Uni</option>
			<option value='choix4'>Canada</option>
			<option value='choix5'>Etats-Unis</option>
			<option value='choix6'>Chine</option>
			<option value='choix7'>Japon</option>
		</select>
		</li>
		<li>
		N° de carte : 
		<input type='text' name='carte' class='field-style field-full align-none' onblur='verifcarte(this)' />
		</li>
		<li>
		Modes de paiement : 
		<select name='choix'>
			<option value='choix1'>VISA</option>
			<option value='choix2'>MasterCard</option>
		</select>
		</li>
		<li>
		Date d'expiration :
		</li>
		<li>
		<input type='text' name='mois' class='field-style field-split align-left' onblur='verifmois(this)' placeholder='Mois' size=5/>/<input type='text' name='annee' class='field-style field-split align-right' onblur='verifannee(this)' placeholder='année' size=5 />
		</li>
		<li>
		Cryptogramme visuel :
		<input type='text' name='crypto' class='field-style field-full align-none' onblur='verifcrypto(this)' size=5/><br><br>
		</li>
		<li>
		<input type='submit' value='Valider' />
		</li>
		</ul>
		</p>
		</form>
		";
		}
		else{
			$html.=
			"<h3>URL de Cagnotte : https://webetu.iutnc.univ-lorraine.fr".$app->urlFor('cagnotte',array('id'=>$_SESSION['id_client'])).
			"</h3><h3>URL de gestion : https://webetu.iutnc.univ-lorraine.fr".$app->urlFor('gestion')."</h3>";
			unset($_SESSION['id_client']);
		}
		
		return $html;
	}
	
	public function enregistrement_paiement(){
		$app=\Slim\Slim::getInstance();
		$html="
		<h2 class='titre_vali'>Paiement validé</h2>
		<h3>URL de gestion :  https://webetu.iutnc.univ-lorraine.fr".$app->urlFor('gestion')."</h3>
		<h3>URL Cadeaux :  https://webetu.iutnc.univ-lorraine.fr".$app->urlFor('cadeaux',array('id'=>$_SESSION['id_client']))."</3>
		";
		
		return $html;
	}
	
	public function entete(){
		$app=\Slim\Slim::getInstance();
		$html='<header>
					<ul id="nav">
						<li><a href="'.$app->urlFor('catalogue').'">Catalogue</a></li>
						<li><a href="'.$app->urlFor('cat').'">Categories</a></li>
					</ul>
			  </header>';
		
		return $html;
	}
	
	public function render($selecteur)
	{
		$app=\Slim\Slim::getInstance();
		$entete=$this->entete();
		$css=$app->urlFor('css');
		$js=$app->urlFor('js');
		$html;
		
		switch($selecteur)
		{
			case self::PANIER:
			$html=$this->panier();
			break;
			
			case self::SUPP:
			$html=$this->panier();
			break;
			
			case self::DIMI:
			$html=$this->panier();
			break;
			
			case self::VALI:
			$html=$this->validation();
			break;
			
			case self::INSCRIP:
			$html=$this->saisie();
			break;
			
			case self::RECAP:
			$html=$this->resumer();
			
			break;
			
			case self::URL:
			$html=$this->enregistrement_paiement();
			break;
			
			
		}
		
		$structure= <<<END
			<!DOCTYPE html>
				<html>
					<head>
						<meta charset="UTF-8">
						<title>Panier</title>
						<link rel="stylesheet" href="$css/accueil.css">
						<script type="text/javascript" src="$js/accueil.js"></script>
					</head>
					<body>
						$entete
						$html
						</body>
						</html>
END;
		
		echo $structure;
	}
}



?>