<?php

namespace giftbox\vue; 

class VueCadeaux{
	
	const CAD=1;
	protected $t;
	
	function __construct($liste,$mess=null)
	{
		$this->t=$liste;
		$this->mess=$mess;
	}
	
	public function affichage_coffret(){
		$app=\Slim\Slim::getInstance();
		$html="
		<table cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>quantité</th>
				</tr>";
		foreach($this->t as $k1=>$v1){
				$html.="<tr>
					<td>".$v1->nom.'<br>'."</td>
					<td>".$v1->descr.'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1->img."'><br></td>
					<td>".$v1->quantite."<br></td>
					</tr>";
		}
		foreach($this->mess as $k=>$v){
			$html.="</table><h3>Message : ".$v->message."</h3>";
		}
		return $html;
	}
	
	public function entete(){
		$app=\Slim\Slim::getInstance();
		$html='<header>
					<ul id="nav">
						<li><a href="'.$app->urlFor('catalogue').'">Catalogue</a></li>
						<li><a href="'.$app->urlFor('cat').'">Categories</a></li>
					</ul>
			  </header>';
		
		return $html;
	}
	
	public function render($selecteur)
	{
		$app=\Slim\Slim::getInstance();
		$entete=$this->entete();
		$css=$app->urlFor('css');
		$html;
		
		switch($selecteur)
		{
			case self::CAD:
			$html=$this->affichage_coffret();
			break;
			
		}
		
		$structure= <<<END
			<!DOCTYPE html>
				<html>
					<head>
						<meta charset="UTF-8">
						<title>Cadeaux</title>
						<link rel="stylesheet" href="$css/accueil.css">
					</head>
					<body>
						$entete
						$html
						</body>
						</html>
END;
		
		echo $structure;
	}
	
}

?>