<?php
namespace giftbox\vue; 
class VueCatalogue {
	
	const LISTPRESTA=3;
	const PRESTA=5;
	const CATPRESTA=4;
	const CAT=2;
	const NOTE=6;
	const N=7;
	const ACC=8;
	const CONNEX=9;
	const GES=10;
	const ADJ=11;
	const EN=12;
	const ERR=13;
	const SUPP=14;
	const DESAC=15;
	const LISTPRESTATRIE=16;
	protected $t;
	
	function __construct($liste,$act=null,$res=null,$heb=null)
	{
		$this->t=$liste;
		$this->act=$act;
		$this->res=$res;
		$this->heb=$heb;
	}
	
	
	public function all_presta()
	{
		$app=\Slim\Slim::getInstance();
		$html='		
		<h1>Toutes les prestations</h1>
		<a href="'.$app->urlFor('cataloguetrie').'"><button class="B1">Trier par prix croissant</button></a>
		<table ">	
		<thead>
				<tr>
					<th>image</th>
					<th>Nom</th>
					<th>Prix</th>
					<th>Note</th>
					<th>Donnez une note</th>
				</tr>
		</thead>	';
				
		foreach($this->t as $v)
		{
			$html.='
			
				<tr onclick="document.location=\''.$app->urlFor('prestaid',array('id'=>$v['id'])).'\'">
					<td><img src="'.$app->urlFor('img').$v['img'].'" /></td>
					<td>'.$v['nom'].'</td>
					<td>'.$v['prix'].'€</td>
					<td>'.$v['note'].'/5</td>
					<td><a href="'.$app->urlFor('affn',array('id'=>$v['id'])).'"><img class="note" src="'.$app->urlFor('img').'etoile.jpg"/></a></td>
				</tr>';
		}
		
		$html.='</table>';
		return $html;
	}
	
	public function presta_cat($cat)
	{
		$app=\Slim\Slim::getInstance();
		$html='	
		<h1>'.$cat.'</h1>	
		<ul>';
			
		foreach($this->t as $v)
		{
			$html.='
			<a href="'.$app->urlFor('prestaid',array('id'=>$v['id'])).'">
				<tr>
					<li>'.$v['nom'].'</td>	
				</tr>
			</a>';
		}
		$html.='</ul>';
		return $html;
	}
	
	public function Details($val)
	{
		$app=\Slim\Slim::getInstance();
		$html='
		<table>
			<thead>
				<tr>
					<th>Image</th>
					<th>Description</th>
					<th>Prix</th>
					<th>Note</th>
					<th>Donnez une note</th>
				</tr>
		</thead>	
			<tr>
				<td><img src="'.$app->urlFor('img').$val['img'].'"/></td>
				<td>Description :'.$val['descr'].'</td>
				<td>'.$val['prix'].'€</td>
				<td>'.$val['note'].'/5</td>
				<td><a href="'.$app->urlFor('affn',array('id'=>$val['id'])).'"><img class="note" src="'.$app->urlFor('img').'etoile.jpg"/></a></td>
				<td><a href="'.$app->urlFor('ajout',array('id'=>$val['id'])).'"><button class="B1">Ajouter au Panier </button></a></td>
			</tr>
				
		</table>';
		
		return $html;
	}
	
	public function categories()
	{
		$app=\Slim\Slim::getInstance();
		$html='<ul>';
		foreach($this->t as $v){
			$html.=
				'<tr onclick="document.location=\''.$app->urlFor('catid',array('id'=>$v['id'])).'\'">
					<li><a href="'.$app->urlFor('catid',array('id'=>$v['id'])).'">'.$v['nom'].'</a></li>
				</tr>';
		}
		$html.='</ul>';
		return $html;
	}
	
	public function note(){
		$app=\Slim\Slim::getInstance();
		$html='<h2 class="titre">Notation</h2>
			<h3>Donnez une note entre 0 et 5</h3>
			<form  onblur="verifn(this)"  class="form-style-9" action="'.$app->urlFor('note').'"method="post">
			<ul>
			<li>
			<input type="number" name="note" onblur="verifnote(this)" class="field-style field-full align-none" placeholder="Note" />
			</li>
			<li>
			<input type="submit" value="Valider" />
			</li>
			</ul>
			</form>';
			
			return $html;
	}
	
	public function devant(){
		$app=\Slim\Slim::getInstance();
		$html="<h1>Meilleures Prestations</h1>
		<h2 class='titre'>Attention</h2>
		<table>
			<thead>
				<tr>
					<th>Nom</th>
					<th>Description</th>
					<th>Image</th>
					<th>Prix</th>
					<th>Note</th>
				</tr>
		</thead>
		<tr onclick='document.location=\"".$app->urlFor('prestaid',array('id'=>$this->t->id))."\"'>
			<td>".$this->t->nom.'<br>'."</td>
			<td>".$this->t->descr.'<br>'."</td>		
			<td><img src='".$app->urlFor('img').$this->t->img."'><br></td>
			<td>".$this->t->prix."€<br></td>
			<td>".$this->t->note."/5</td>
		</tr>
		</table>
		<h2 class='titre'>Activité</h2>
		<table>
			<thead>
				<tr>
					<th>Nom</th>
					<th>Description</th>
					<th>Image</th>
					<th>Prix</th>
					<th>Note</th>
				</tr>
		</thead>
		<tr onclick='document.location=\"".$app->urlFor('prestaid',array('id'=>$this->act->id))."\"'>
					<td>".$this->act->nom.'<br>'."</td>
					<td>".$this->act->descr.'<br>'."</td>		
					<td><img src='".$app->urlFor('img').$this->act->img."'><br></td>
					<td>".$this->act->prix."€<br></td>
					<td>".$this->act->note."/5</td></tr>
		</table>
		<h2 class='titre'>Restaurant</h2>
		<table>
			<thead>
				<tr>
					<th>Nom</th>
					<th>Description</th>
					<th>Image</th>
					<th>Prix</th>
					<th>Note</th>
				</tr>
		</thead>
				<tr onclick='document.location=\"".$app->urlFor('prestaid',array('id'=>$this->res->id))."\"'>
					<td>".$this->res->nom.'<br>'."</td>
					<td>".$this->res->descr.'<br>'."</td>		
					<td><img src='".$app->urlFor('img').$this->res->img."'><br></td>
					<td>".$this->res->prix."€<br></td>
					<td>".$this->res->note."/5</td></tr>
		</table>
		<h2 class='titre'>Hébergement</h2>
		<table>
			<thead>
				<tr>
					<th>Nom</th>
					<th>Description</th>
					<th>Image</th>
					<th>Prix</th>
					<th>Note</th>
				</tr>
		</thead>
				<tr onclick='document.location=\"".$app->urlFor('prestaid',array('id'=>$this->heb->id))."\"'>
					<td>".$this->heb->nom.'<br>'."</td>
					<td>".$this->heb->descr.'<br>'."</td>		
					<td><img src='".$app->urlFor('img').$this->heb->img."'><br></td>
					<td>".$this->heb->prix."€<br></td>
					<td>".$this->heb->note."/5</td></tr>
					</table>";
		
		return $html;
	}
	public function connexion(){
	$app=\Slim\Slim::getInstance();
		$html="
			<form action='".$app->urlFor('choixad')."' onsubmit='return verifconnexion(this)' class='form-style-9' method='post'>
			<p>
			<ul>
			<li>
			Veuillez taper votre adresse mail:
			</li>
			<li>
			<input type='email' name='mail' size='30' onblur='verifMail(this)' class='field-style field-full align-none' placeholder='Email'/>
			</li>
			<li>
			Veuillez taper votre mot de passe:
			</li>
			<li>
			<input type='password' name='mdp'  onblur='verifmdp(this)' class='field-style field-full align-none' placeholder='Mot de passe'/>
			</li>
			<li>
			<input type='submit' value='Valider' />
			</li>
			</ul>
			</p>
			</form>
		";
		
		return $html;
	}
	
	public function choix(){
		$app=\Slim\Slim::getInstance();
		$html='
		<ul>
			<a href="'.$app->urlFor('adj').'"><button class="B4">ajout prestation</button></a>
			<a href="'.$app->urlFor('suppp').'"><button class="B4">supprimer prestation</button></a>
			<a href="'.$app->urlFor('desacp').'"><button class="B4">désactiver prestation</button></a>
		</ul>	
		';
		
		return $html;
		
	}
	
	public function adjpresta(){
		$app=\Slim\Slim::getInstance();
		$html='<h2 class="titre">Ajout de prestation</h2>
			<form class="form-style-9" action="'.$app->urlFor('enregistrementAdj').'" enctype="multipart/form-data" method="post">
			<ul>
			<li>
			<input type="text" name="nom" class="field-style field-full align-none" placeholder="Nom" />
			</li>
			<li>
			<input type="text" name="cat"  class="field-style field-split align-left" placeholder="Catégorie" />
			<input type="number" name="prix" class="field-style field-split align-right" placeholder="Prix" />
			</li>
			<li>
			<textarea type="text" name="description" placeholder="description" class="field-style"></textarea>
			</li>
			<li>
			<input type="file" name="image" class="field-style field-split align-left" placeholder="Image" />
			</li>
			<br>
			<li>
			<input type="submit" value="Valider" />
			</li>
			</ul>
			</form>';
			return $html;
	}
	
	public function enregistrement(){
		return $html='<h3>action enregistrer</h3>';
	}
	
	public function erreur(){
		return $html='<h3>'.$this->t.'</h3>';
	}
	
	public function supprimmer(){
		$app=\Slim\Slim::getInstance();
		$html='<h2 class="titre">Supprimmer une prestation</h2>
			<form class="form-style-9" action="'.$app->urlFor('enregistrementSupp').'" method="post">
			<ul>
			<li>
			<input type="text" name="nom" class="field-style field-full align-none" placeholder="Nom de la Prestation" />
			</li>
			<li>
			<input type="submit" value="Valider" />
			</li>
			</ul>
			</form>';
			return $html;
	}
	
	public function desactiver(){
		$app=\Slim\Slim::getInstance();
		$html='<h2 class="titre">Désactiver une prestation</h2>
			<form class="form-style-9" action="'.$app->urlFor('enregistrementDesac').'" method="post">
			<ul>
			<li>
			<input type="text" name="nom" class="field-style field-full align-none" placeholder="Nom de la Prestation" />
			</li>
			<li>
			<input type="submit" value="Valider" />
			</li>
			</ul>
			</form>';
			return $html;
	}
	
	public function entete(){
		$app=\Slim\Slim::getInstance();
		$html='<header>
					<ul id="nav">
						<li><a href="'.$app->urlFor('catalogue').'">Catalogue</a></li>
						<li><a href="'.$app->urlFor('cat').'">Categories</a></li>
						<li><a href="'.$app->urlFor('admin').'"><button class="B4">Gestion</button></a></li>
					</ul>
			  </header>';
		
		return $html;
	}
	
	
	public function render($selecteur, $cat=null){
		$app=\Slim\Slim::getInstance();
		$entete=$this->entete();
		$css=$app->urlFor('css');
		$js=$app->urlFor('js');
		$html;
		
		switch($selecteur)
		{
			
			case self::LISTPRESTA:
			$html=$this->all_presta();
			break;
			
			case self::LISTPRESTATRIE:
			$html=$this->all_presta();
			break;
			
			case self::PRESTA:
			$html=$this->Details($this->t[0]);
			break;
			
			case self::CATPRESTA:
			$html=$this->presta_cat($cat);
			break;
			
			case self::CAT:
			$html=$this->categories();
			break;
			
			case self::NOTE:
			$html=$this->note();
			break;
			
			case self::N:
			$html=$this->all_presta();
			break;
			
			case self::ACC:
			$html=$this->devant();
			break;
			
			case self::CONNEX:
			$html=$this->connexion();
			break;
			
			case self::GES:
			$html=$this->choix();
			break;
			
			case self::ADJ:
			$html=$this->adjpresta();
			break;
			
			case self::EN:
			$html=$this->enregistrement();
			break;
			
			case self::ERR:
			$html=$this->erreur();
			break;
			
			case self::SUPP:
			$html=$this->supprimmer();
			break;
			
			case self::DESAC:
			$html=$this->desactiver();
			break;
		}
		
		
		$structure= <<<END
			<!DOCTYPE html>
				<html>
					<head>
						<meta charset="UTF-8">
						<title>Accueil</title>
						<link rel="stylesheet" href="$css/accueil.css">
						<script type="text/javascript" src="$js/accueil.js"></script>
					</head>
					<body>
						$entete
						$html
						</body>
						</html>
END;
		
		echo $structure;
	}

}

?>

