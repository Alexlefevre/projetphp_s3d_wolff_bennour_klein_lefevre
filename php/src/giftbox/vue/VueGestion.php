<?php

namespace giftbox\vue; 

class VueGestion{
	
	const DONNEE=1;
	const CONNEX=2;
	const CONNEXC=3;
	const VALI=4;
	const SUPP=5;
	const ERR=6;
	protected $t;
	protected $etat;
	
	function __construct($liste,$etat=null,$mont=null,$res=null)
	{
		$this->t=$liste;
		$this->etat=$etat;
		$this->mont=$mont;
		$this->res=$res;
	}
	
	public function connexion(){
		$app=\Slim\Slim::getInstance();
		$html="
			<form action='".$app->urlFor('coffret')."' onsubmit='return verifconnexion(this)' class='form-style-9' method='post'>
			<p>
			<ul>
			<li>
			Veuillez taper votre adresse mail:
			</li>
			<li>
			<input type='email' name='mail' size='30' onblur='verifMail(this)' class='field-style field-full align-none' placeholder='Email'/>
			</li>
			<li>
			Veuillez taper votre mot de passe:
			</li>
			<li>
			<input type='password' name='mdp'  onblur='verifmdp(this)' class='field-style field-full align-none' placeholder='Mot de passe'/>
			</li>
			<li>
			<input type='submit' value='Valider' />
			</li>
			</ul>
			</p>
			</form>
		";
		
		return $html;
	}
	
	public function affichage_coffret(){
		$app=\Slim\Slim::getInstance();
		$tp=0;
		$html="
		<table cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($this->t as $v1){
				$html.="<tr>
					<td>".$v1['nom'].'<br>'."</td>
					<td>".$v1['descr'].'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1['img']."'><br></td>
					<td>".$v1['prix']."€<br></td>
					<td>".$v1['quantite']."<br></td>
					</tr>";
				$tp=$tp+$v1['prix']*$v1['quantite'];
		}
		$html.=" </table>
		<h3>prix total : ".$tp."€</h3>
		<h3>état de l'URL cadeau : ".$this->etat."<h3>
		";
		return $html;
	}
	
	public function affichage(){
		$app=\Slim\Slim::getInstance();
		$tp=0;
		$html="
		<table cellspacing='0'>
				<tr>
					<th>nom</th>
					<th>description</th>
					<th>image</th>
					<th>prix</th>
					<th>quantité</th>
				</tr>";
		foreach($this->t as $v1){
				$html.="<tr>
					<td>".$v1['nom'].'<br>'."</td>
					<td>".$v1['descr'].'<br>'."</td>
					<td><img src='".$app->urlFor('img').$v1['img']."'><br></td>
					<td>".$v1['prix']."€<br></td>
					<td>".$v1['quantite']."<br></td>
					<td><a href='".$app->urlFor('supprimer',array('id'=>$v1->id))."'><button class='B2'>supprimer</button></a></td>
					</tr></tr>";
				$tp=$tp+$v1['prix']*$v1['quantite'];
		}
		$html.=" </table>
		<h3>prix total : ".$tp."€</h3>
		<h3>état de l'URL cadeau : ".$this->etat."<h3>
		<h3>montant déjà payé : ".$this->mont."</h3><br>
		".$this->res."
		";
		return $html;
	}
	
	public function cloture(){
		$html='<h1>Coffret valider</h1>';
		return $html;
	}
	
	public function erreur(){
		return $html='<h3>'.$this->t.'</h3>';
	}
	
	public function entete(){
		$app=\Slim\Slim::getInstance();
		$html='<header>
					<ul id="nav">
						<li><a href="'.$app->urlFor('catalogue').'">Catalogue</a></li>
						<li><a href="'.$app->urlFor('cat').'">Categories</a></li>
					</ul>
			  </header>';
		
		return $html;
	}
	
	public function render($selecteur)
	{
		$app=\Slim\Slim::getInstance();
		$css=$app->urlFor('css');
		$entete=$this->entete();
		$html;
		
		switch($selecteur)
		{
			case self::DONNEE:
			$html=$this->connexion();
			break;
			
			case self::CONNEX:
			$html=$this->affichage_coffret();
			break;
			
			case self::CONNEXC:
			$html=$this->affichage();
			break;
			
			case self::SUPP:
			$html=$this->affichage();
			break;
			
			case self::VALI:
			$html=$this->cloture();
			break;
			
			case self::ERR:
			$html=$this->erreur();
			break;
			
		}
		
		$structure= <<<END
			<!DOCTYPE html>
				<html>
					<head>
						<meta charset="UTF-8">
						<title>Gestion</title>
						<link rel="stylesheet" href="$css/accueil.css">
					</head>
					<body>
						$entete
						$html
						</body>
						</html>
END;
		
		echo $structure;
	}
}


?>