<?php
namespace giftbox\controller;

use \giftbox\vue\VueGestion;
use \giftbox\models\Coffret;
use \giftbox\models\Client;
use \giftbox\models\Authentification;

class ControllerGestion{
	
	public function donnee(){
		$inf=null;
		$d=new VueGestion($inf);
		$d->render(VueGestion::DONNEE);
	}
	
	public function coffret(){
		$app=\Slim\Slim::getInstance();
		$hash=null;
		$h=Client::where('email','=',$_POST['mail'] )->get();
		foreach($h as $k=>$v){
			$hash=$v->mdp;
		}
			$r=Authentification::authentification($_POST['mail'],$_POST['mdp'],$hash);
			Authentification::loadProfile($_POST['mail']);
		if($r==null){
		foreach($_SESSION['coffret'] as $k1=>$v1){
			$etat=$v1->etat;
			$mont=$v1->montant_paye;
			$mode=$v1->modep;
			$totale=$v1->montant_totale;
			if($mode=='Classique'){
			$rid=new VueGestion($_SESSION['profil'],$etat);
			$rid->render(VueGestion::CONNEX);
			}
			else{
			if($mont>=$totale){
				$z=Client::find($v1->id);
				$z->coffret='payes';
				$z->save();
				$res="<a href='".$app->urlFor('ValiC')."'><button class='B2'>Valider</button></a>";
			}
			else{
				$res='Montant insuffisant pour clôturer la cagnotte';
			}
			$rid=new VueGestion($_SESSION['profil'],$etat,$mont,$res);
			$rid->render(VueGestion::CONNEXC);
			
			}
		}
		}
		else{
			$p=new VueGestion($r);
			$p->render(VueGestion::ERR);
		}
	unset($_SESSION['coffret']);
	unset($_SESSION['profil']);
	}
	
	public function supprimer($id){
		$i=null;
		$pay=null;
		$totale=0;
		$b=Coffret::select('client_id','prix','quantite')->where('id','=',$id)->get();
		foreach($b as $k1=>$v1){
			$i=$v1->client_id;
			$p=$v1->prix;
			$q=$v1->quantite;
			$tp=Client::select('montant_totale')->where('id','=',$i)->get();
			foreach($tp as $k=>$v){
				$tot=$v->montant_totale;
			}
			$paye=$q*$p;
			$totale=$tot-$paye;
			
			$t=Client::find($i);
			$t->montant_totale=$totale;
			$t->save();
		}
			$mp=Client::select('montant_paye')->where('id','=',$i)->get();
			foreach($mp as $k=>$v){
			$pay=$v->montant_paye;
			echo $mp;
			}
			$va=Coffret::where('client_id','=',$i)->where('id','!=',$id)->get();
			Coffret::destroy($id);
			$supp=new VueGestion($va,$pay);
			$supp->render(VueGestion::SUPP);
	}
	
	public function validation(){
		$a=null;
		$p=new VueGestion($a);
		$p->render(VueGestion::VALI);
	}
}
?>