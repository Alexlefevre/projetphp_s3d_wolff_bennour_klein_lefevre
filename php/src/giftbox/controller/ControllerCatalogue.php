<?php

namespace giftbox\controller;

use \giftbox\models\Categorie;
use \giftbox\models\Prestation;
use \giftbox\models\Note;
use \giftbox\models\Client;
use \giftbox\models\Authentification;
use \giftbox\vue\VueCatalogue;

class ControllerCatalogue {
	
	public function debut() {
	}
	
	public function Presta($id){
		$p=Prestation::where('id','like',$id)->where('visible','=',true)->get();
		$presta = new VueCatalogue($p);
		$presta->render(VueCatalogue::PRESTA);
	}
	
	public function catalogue(){
		$listPresta = Prestation::where('visible','=',true)->get();
		$catalogue=new VueCatalogue($listPresta);
		$catalogue->render(VueCatalogue::LISTPRESTA);
	}
	
	public function ListePresta($id){
		$n=Prestation::where('cat_id','like',$id)->where('visible','=',true)->get();
		$v=Categorie::where('id','like',$id)->first()->nom;
		$lp = new VueCatalogue($n);
		$lp->render(VueCatalogue::CATPRESTA,$v);
	}
	
	public function listeCat(){
		$c=Categorie::get();
		$cat = new VueCatalogue($c);
		$cat->render(VueCatalogue::CAT);
		
	}
	
	public function trierall(){
		$listPresta = Prestation::where('visible','=',true)->orderby('prix')->get();
		$catalogue=new VueCatalogue($listPresta);
		$catalogue->render(VueCatalogue::LISTPRESTATRIE);
	}
	
	public function aff_note($id){
		$n=null;
		if(!isset($_SESSION['note'])){
			$_SESSION['note']=$id;
		}
		$note = new VueCatalogue($n);
		$note->render(VueCatalogue::NOTE);
	}
	
	public function notation(){
		$n=new \giftbox\models\Note;
		if($_POST['note']>5){
			$_POST['note']=5;
		}
		if($_POST['note']<0){
			$_POST['note']=0;
		}
		$n->prestation_id=$_SESSION['note'];
		$n->note=$_POST['note'];
		$n->save();
		$ln=Note::where('prestation_id','=',$_SESSION['note'])->get();
		$moy=0;
		foreach($ln as $k=>$v){
			$moy+=$v->note;
		}
		if(sizeof($ln)==0){
			$note=$_POST['note'];
		}
		else{
			$note=$moy/sizeof($ln);
		}
		$t=Prestation::find($_SESSION['note']);
		$t->note=$note;
		$t->save();
		$f=Prestation::where('visible','=',true)->get();
		$catalogue=new VueCatalogue($f);
		$catalogue->render(VueCatalogue::N);
		unset($_SESSION['note']);
	}
	
	public function Accueil(){
		$Attention=Prestation::where('cat_id','=',1)->max('note');
		if($Attention==0){
			$cat1=Prestation::where('cat_id','like',1)->first();
		}else{
			$cat1=Prestation::where('note','=',$Attention)->first();
		}
		
		$Activit�=Prestation::where('cat_id','like',2)->max('note');
		if($Activit�==0){
			$cat2=Prestation::where('cat_id','like',2)->first();
		}else{
			$cat2=Prestation::where('note','=',$Activit�)->first();
		}
		
		$Restauration=Prestation::where('cat_id','=',3)->max('note');
		if($Restauration==0){
			$cat3=Prestation::where('cat_id','like',3)->first();
		}else{
			$cat3=Prestation::where('note','=',$Restauration)->first();
		}
		
		$Hebergement=Prestation::where('cat_id','=',4)->max('note');
		if($Hebergement==0){
			$cat4=Prestation::where('cat_id','like',4)->first();
		}else{
			$cat4=Prestation::where('note','=',$Hebergement)->first();
		}
		
		$acc=new VueCatalogue($cat1,$cat2,$cat3,$cat4);
		$acc->render(VueCatalogue::ACC);
	}
	
	public function login(){
		$co=null;
		$d=new VueCatalogue($co);
		$d->render(VueCatalogue::CONNEX);
	}
	
	public function Administration(){
		$app=\Slim\Slim::getInstance();
		$lvl=null;
		$hash=null;
		$h=Client::where('email','=',$_POST['mail'] )->get();
		foreach($h as $k=>$v){
			$hash=$v->mdp;
			$lvl=$v->droit;
		}
		$r=Authentification::authentification($_POST['mail'],$_POST['mdp'],$hash);
		Authentification::loadProfile($_POST['mail']);
		$f=Authentification::checkAccessRights($lvl);
		$v=null;
		if($r==null && $f==null){
			$ad=new VueCatalogue($v);
			$ad->render(VueCatalogue::GES);
			unset($_SESSION['coffret']);
			unset($_SESSION['profil']);
		}
		else{
			if($r!==null){
			$p=new VueCatalogue($r);
			$p->render(VueCatalogue::ERR);
			}
			if($f!=null){
				$p=new VueCatalogue($f);
				$p->render(VueCatalogue::ERR);
			}
		}
	}
	
	public function adjpinf(){
		$inf=null;
		$p=new VueCatalogue($inf);
		$p->render(VueCatalogue::ADJ);
	}
	
	public function newpresta(){
		if(isset($_FILES['image'])){
			$dossier = 'img/';
			$fichier = basename($_FILES['image']['name']);
			$extensions = array('.jpg');
			$extension = strrchr($_FILES['image']['name'], '.'); 
			$taille_max = 100000;
			$taille = filesize($_FILES['image']['tmp_name']);
			if(!in_array($extension, $extensions)){
				$erreur = 'Vous devez uploader un fichier de type jpg';
				}
			if($taille>$taille_max){
				$erreur = 'Le fichier est trop gros';
				}
			if(!isset($erreur)){	
			move_uploaded_file($_FILES['image']['tmp_name'],$dossier.$fichier);
			$p=new \giftbox\models\Prestation;
			$p->nom=$_POST['nom'];
			$p->descr=$_POST['description'];
			$p->cat_id=$_POST['cat'];
			$p->img=$_FILES['image']['name'];
			$p->prix=$_POST['prix'];
			$p->note=0;
			$p->visible=true;
			$p->save();
			$inf=null;
			$p=new VueCatalogue($inf);
			$p->render(VueCatalogue::EN);
			}
			else{
				$p=new VueCatalogue($erreur);
				$p->render(VueCatalogue::ERR);
			}
		}
		
	}
	
	public function suppPresta(){
		$inf=null;
		$p=new VueCatalogue($inf);
		$p->render(VueCatalogue::SUPP);
	}
	
	public function supp(){
		$p=Prestation::select('id')->where('nom','=',$_POST['nom'])->get();
		$id=null;
		foreach($p as $k=>$v){
			$id=$v->id;
		}
		if($id==null){
			$erreur='nom incorrect';
			$p=new VueCatalogue($erreur);
			$p->render(VueCatalogue::ERR);
		}
		else{
		Prestation::destroy($id);
		$inf=null;
		$p=new VueCatalogue($inf);
		$p->render(VueCatalogue::EN);
		}
	}
	
	public function desacPresta(){
		$inf=null;
		$p=new VueCatalogue($inf);
		$p->render(VueCatalogue::DESAC);
	}
	
	public function desac(){
		$p=Prestation::select('id')->where('nom','=',$_POST['nom'])->get();
		$id=null;
		foreach($p as $k=>$v){
			$id=$v->id;
		}
		if($id==null){
			$erreur='nom incorrect';
			$p=new VueCatalogue($erreur);
			$p->render(VueCatalogue::ERR);
		}
		else{
		$m=Prestation::find($id);
		$m->visible=false;
		$m->save();
		$inf=null;
		$p=new VueCatalogue($inf);
		$p->render(VueCatalogue::EN);
		}
	}
	
	
	
}