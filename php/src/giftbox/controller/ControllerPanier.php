<?php

namespace giftbox\controller;

use \giftbox\vue\VuePanier;
use \giftbox\models\Prestation;
use \giftbox\models\Client;
use \giftbox\models\Categorie;
use \giftbox\models\Authentification;

class ControllerPanier{
	
	
	public function AjoutPanier($id){
		if(!isset($_SESSION['panier'])){
			$_SESSION['panier']=array();
		}
		$p=Prestation::where('id','like',$id)->first();
		$pp=Prestation::where('id','like',$id)->get();
		foreach($pp as $k=>$v){
			$cat=$v->cat_id;
		}
		$c=Categorie::select('nom')->where('id','=',$cat)->get();
		foreach($c as $k=>$v){
			$nom=$v->nom;
		}
		$res = false;
		foreach($_SESSION['panier'] as $value){
			if($value->id == $p->id){
				$value->quantite++;
				$res = true;
			}
		}
		if(!$res){
			$p->quantite = 1;
			$p->categorie=$nom;
			array_push($_SESSION['panier'], $p);
		}
		$panier= new VuePanier($p);
		$panier->render(VuePanier::PANIER);
	}
	
	public function supprimer($p){
			$a=NULL;
			unset($_SESSION['panier'][$p]);
			$_SESSION['panier'] = array_values($_SESSION['panier']);
			$supp=new VuePanier($a);
			$supp->render(VuePanier::SUPP);
	}
	
	public function diminuer($id){
		$a=NULL;
		foreach($_SESSION['panier'] as $value){
			if($value->id == $id){
				if($value->quantite>1){
				$value->quantite--;
				}
			}
		}
		$dim=new VuePanier($a);
		$dim->render(VuePanier::DIMI);
	}
	
	public function vali(){
		$app=\Slim\Slim::getInstance();
		$r=null;
		if(sizeof($_SESSION['panier'])>1){
			$a=$_SESSION['panier'][0]->cat_id;
			for($i=1;$i<sizeof($_SESSION['panier']);$i++){
				if($a!=$_SESSION['panier'][$i]->cat_id){
					$r="<a href='".$app->urlFor('inscription')."'><button class='B2'>Inscription</button></a>";
				}
				else{
				$r='PAIEMENT INVALIDE<br>Le coffret doit contenir au moins deux catégorie de produit differente.';
				}
			}
		}
		else{
			$r='PAIEMENT INVALIDE<br>Le coffret doit contenir au moins deux produit';
		}
		$vali=new VuePanier($r);
		$vali->render(VuePanier::VALI);
	}
	
	public function inscription(){
		$inf=null;
		$p=new VuePanier($inf);
		$p->render(VuePanier::INSCRIP);
	}
	
	public function recap(){
		$m=Client::select('email')->get();
		$res=false;
		foreach($m as $k1=>$v1){
			if($_POST['mail']==$v1->email){
				$res=true;
			}
		}
			if($res==true){
				$inf=null;
				$ins=new VuePanier($inf);
				$ins->render(VuePanier::INSCRIP);
				echo'adresse mail déjà utilisé';
			}
			else{
				$r=null;
				$tp=0;
				$v=new \giftbox\models\Client;
				$v->nom=$_POST['nom'];
				$v->prenom=$_POST['prenom'];
				$v->message=$_POST['message'];
				$v->modep=$_POST['paiement'];
				$v->coffret='impayés';
				$v->save();
				if(!isset($_SESSION['nom'])){
							$_SESSION['nom']=$_POST['nom'];
						}
				for($i=0;$i<sizeof($_SESSION['panier']);$i++){
					$e=Client::select('id')->where('nom','=',$_SESSION['nom'])->get();
						foreach($e as $k1=>$v1){
							$z=new \giftbox\models\Coffret;
							$z->produit_id=$_SESSION['panier'][$i]->id;
							$z->client_id=$v1->id;
							$z->nom=$_SESSION['panier'][$i]->nom;
							$z->descr=$_SESSION['panier'][$i]->descr;
							$z->cat_id=$_SESSION['panier'][$i]->cat_id;
							$z->img=$_SESSION['panier'][$i]->img;
							$z->prix=$_SESSION['panier'][$i]->prix;
							$z->quantite=$_SESSION['panier'][$i]->quantite;
							$z->save();
							$tp=$tp+$_SESSION['panier'][$i]->prix*$_SESSION['panier'][$i]->quantite;
						}
				}
				$v->montant_totale=$tp;
				$v->save();
				$e=Client::select('id')->where('nom','=',$_POST['nom'])->get();
				foreach($e as $k1=>$v1){
					$j=$v1->id;
				}
				$mail=$_POST['mail'];
				$mdp=$_POST['mdp'];
				try{
					Authentification::createUser($mail,$mdp,$j);
				}catch (AuthException $ae) {
					echo "access denied !<br>";
				}
				
				if(!isset($_SESSION['id_client'])){
					$_SESSION['id_client']=$j;
				}	
				if($_POST['paiement']=='Cagnotte'){
					$t=Client::find($j);
					$t->etat='transmis au destinataire';
					$t->save();
				}		
				$p=new VuePanier($r);
				$p->render(VuePanier::RECAP);
				unset($_SESSION['panier']);
				unset($_SESSION['nom']);
			}
	}
	
	public static function insertion(){
		$m=null;
		$t=Client::find($_SESSION['id_client']);
		$t->coffret='payes';
		$t->etat='transmis au destinataire';
		$t->save();
		$i=new VuePanier($m);
		$i->render(VuePanier::URL);
		
	 }
}
?>