<?php
namespace giftbox\controller;

use \giftbox\vue\VueCagnotte;
use \giftbox\models\Coffret;
use \giftbox\models\Client;

class ControllerCagnotte{

	
	public function recup_coffret($id){
		$coffret=Coffret::where('client_id','=',$id)->get();
			$mp=Client::select('montant_paye')->where('id','=',$id)->get();
			foreach($mp as $k=>$v){
			$pay=$v->montant_paye;
			}
			$c=new VueCagnotte($coffret,$pay,$id);
			$c->render(VueCagnotte::CAGNOTTE);
	}
	
	public function ajouter($id){
		$mp=Client::select('montant_paye')->where('id','=',$id)->get();
		$mont=$_POST['mont'];
		foreach($mp as $k=>$v){
			$pay=$v->montant_paye;
		}
		$totale=$pay+$mont;
		$t=Client::find($id);
		$t->montant_paye=$totale;
		$t->save();
	
		$adj=new VueCagnotte($mont,$totale);
		$adj->render(VueCagnotte::ADJ);
	}
}

?>