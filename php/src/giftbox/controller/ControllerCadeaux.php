<?php
namespace giftbox\controller;

use \giftbox\vue\VueCadeaux;
use \giftbox\models\Coffret;
use \giftbox\models\Client;

class ControllerCadeaux{
	
	public function cadeaux($id){
		$coffret=Coffret::where('client_id','=',$id)->get();
		$mess=Client::select('message')->where('id','=',$id)->get();
		$b=Client::find($id);
		$b->etat='utilisé';
		$b->save();
		$cad=new VueCadeaux($coffret,$mess);
		$cad->render(VueCadeaux::CAD);
	}
}

?>